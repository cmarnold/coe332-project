FROM python:3.6

RUN pip install -r api/requirements.txt

ADD ./api /api
WORKDIR /api

ENTRYPOINT ["python"]
CMD ["run.py"]


