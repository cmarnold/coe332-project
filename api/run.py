import json
import io
from flask import Flask, send_file, request

import data
import jobs

app = Flask(__name__)

def get_start_end():
    """Helper function to return the start and end parameters from the request, supplying defaults if not provided."""
    start = request.args.get('start')
    end = request.args.get('end')
    if start or end:
        if not start:
            start = '0'
        if not end:
            end = '0'
    return start, end

def do_paging(data):
    """Helper function to retrieve and apply limit and offset parameters to a list, data."""
    try:
        limit = int(request.args.get('limit', len(data)))
        offset = int(request.args.get('offset', 0))
    except ValueError:
        return json.dumps({'status': "Error", 'message': 'limit and offset, if provided, must be integers.'})
    return json.dumps(data[offset: limit + offset])

@app.route('/population', methods=['GET'])
def population():
    """Returns the spots data, applying start and end years and paging, if supplied in the request."""
    start, end = get_start_end()
    if start or end:
        return json.dumps(data.get_data(start, end))
    return do_paging(data.get_data())

@app.route('/population/<id>', methods=['GET'])
def population_for_id(id):
    """Return the popuation data point for a single row id."""
    try:
        int(id)
    except ValueError:
        return json.dumps({'status': "Error", 'message': 'id must be an integer.'})
    result = [x for x in data.get_data() if x.get('id') == int(id)]
    return json.dumps(result)

@app.route('/population/year/<year>', methods=['GET'])
def population_for_year(year):
    """Return the population data point for a single year."""
    result = [x for x in data.get_data() if x.get('year') == year]
    return json.dumps(result)

def validate_job():
    """Check that a job request is valid; start and end must be be supplied and be integers."""
    try:
        job = request.get_json(force=True)
    except Exception as e:
        return True, json.dumps({'status': "Error", 'message': 'Invalid JSON: {}.'.format(e)})
    start = job.get('start')
    try:
        job['start'] = int(start)
    except:
        return True, json.dumps({'status': "Error", 'message': 'start parameter must be an integer.'})
    end = job.get('end')
    try:
        job['end'] = int(end)
    except:
        return True, json.dumps({'status': "Error", 'message': 'end parameter must be an integer.'})
    if job['start'] > job['end']:
        return True, json.dumps({'status': "Error", 'message': 'start must be less than or equal to end.'})
    return False, job

@app.route('/jobs', methods=['GET', 'POST'])
def jobs_api():
    """List all jobs and create new jobs."""
    if request.method == 'POST':
        error, job = validate_job()
        if error:
            return job
        return json.dumps(jobs.add_job(job['start'], job['end']))
    else:
        data = jobs.get_all_jobs()
        return do_paging(data)

@app.route('/jobs/<job_id>', methods=['GET', 'DELETE'])
def job(job_id):
    """List and delete a job by job id."""
    if request.method == 'GET':
        return json.dumps(jobs.get_job_by_id(job_id))
    else:
        jobs.delete_by_id(job_id)
        return json.dumps({'status': 'Success', 'message': 'job {} deleted.'.format(job_id)})

@app.route('/jobs/<job_id>/plot', methods=['GET'])
def job_plot(job_id):
    """Returns the plot produced by a job as a binary png file attachment to the response."""
    plot = jobs.get_job_plot(job_id)
    return json.dumps({'status': 'Success', 'message': plot })
    #return json.dumps({'status': "Error", 'message': 'Not implemented.'})

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
