import os
import uuid

from hotqueue import HotQueue
import redis

import matplotlib
# cf. https://stackoverflow.com/questions/37604289/tkinter-tclerror-no-display-name-and-no-display-environment-variable
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from data import get_data

REDIS_IP = os.environ.get('REDIS_IP', '172.17.0.1')
try:
    REDIS_PORT = int(os.environ.get('REDIS_PORT'))
except:
    REDIS_PORT = 6379

# Redis DBs
DATA_DB = 0
QUEUE_DB = 1

rd = redis.StrictRedis(host=REDIS_IP, port=REDIS_PORT, db=DATA_DB)
q = HotQueue("queue", host=REDIS_IP, port=REDIS_PORT, db=QUEUE_DB)

# Job Status
SUBMITTED_STATUS = 'submitted'
IN_PROGRESS = 'in_progress'
COMPLETE_STATUS = 'complete'


# Private functions --

def _generate_jid():
    return str(uuid.uuid4())

def _generate_job_key(jid):
    return 'job.{}'.format(jid)

def _instantiate_job(jid, status, start, end, plot):
    """ Creates a Python object representing a job. """
    if type(jid) == str:
        return {'id': jid,
                'status': status,
                'start': start,
                'end': end,
                'plot': plot
        }
    return {'id': jid.decode('utf-8'),
            'status': status.decode('utf-8'),
            'start': start.decode('utf-8'),
            'end': end.decode('utf-8'),
            'plot': plot.decode('utf-8')
    }

def _save_job(job_key, job_dict):
    """Save a job object in the Redis database."""
    rd.hmset(job_key, job_dict)

def _get_job_by_jobkey(job_key):
    """
    Return a job from the Redis db using the redis key, `job_key`.
    :param job_key:
    :return:
    """
    jid, status, start, end = rd.hmget(job_key, 'id', 'status', 'start', 'end', 'plot')
    if jid:
        return _instantiate_job(jid, status, start, end, plot)
    return None


# Public Job functions --

def get_job_by_id(jid):
    """Returns an existing job with id `jid` from the Redis db or None."""
    return _get_job_by_jobkey(_generate_job_key(jid))

def get_all_jobs():
    """Returns a list of all job objects in the Redis db."""
    result = []
    for job_key in rd.keys():
        result.append(_get_job_by_jobkey(job_key))
    return result


def add_job(start, end, status=SUBMITTED_STATUS):
    """Add a job to the redis queue."""
    jid = _generate_jid()
    job_dict = _instantiate_job(jid, status, start, end)
    _save_job(_generate_job_key(jid), job_dict)
    queue_job(jid)
    print("Job has been queued")
    return job_dict

def update_job_status(jid, status):
    """Update the status of job with job id `jid` to status `status`."""
    job = get_job_by_id(jid)
    if job:
        job['status'] = status
        _save_job(_generate_job_key(jid), job)
    else:
        raise Exception()

def delete_by_id(jid):
    rd.delete(_generate_job_key(jid))


# Queue --

def queue_job(jid):
    q.put(jid)

def finalize_job(jid, file_path):
    """Update the job in the db with status and plot once worker has completed it."""
    job = get_job_by_id(jid)
    job['status'] = COMPLETE_STATUS
    job['plot'] = open(file_path, 'rb').read()
    rd.hmset(jid, job)

def get_job_plot(jid):
    """Returns the plot, as binary data, associated with the job"""
    job = get_job_by_id(jid)
    if not job['status'] == COMPLETE_STATUS:
        return True, "job not complete."
    return False, rd.hmget(jid, 'plot')

# Execute worker
def execute_job(jid):
    """Execute the job. This is the callable that is queued and worked on asynchronously."""
    job = get_job_by_id(jid)
    points = get_data(job['start'], job['end'])
    years = [int(p['year']) for p in points]
    population = [p['population'] for p in points]
    plt.scatter(years, population)
    tmp_file = '/tmp/{}.png'.format(jid)
    plt.savefig(tmp_file, dpi=150)
    finalize_job(jid, tmp_file)
