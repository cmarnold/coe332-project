def get_data(start=None, end=None):
    result = []
    with open('dataset/population.txt', 'r') as f:
        for idx, l in enumerate(f.readlines()):
            if (l != ''):
                year, pop = l.split(',')
                result.append({'id': idx, 'year': year.strip(), 'population': int(pop.strip())})
    if not start:
        start = result[0]['year']
    if not end:
        end = result[len(result)-1]['year']
    return [x for x in result if x.get('year') >= start and x.get('year') <= end]
