# Project Overview

This is meant to show a full, functioning project, and illustrate the final product we are looking for. 

# How to Build

After cloning the project:

`% cd project`

`% docker build -t population-api .`

# How to Run

`% docker-compose up`

# API Documentation


## Getting population data

### [GET] /population 

> Get all the population data

### [GET] /population/<id> 

> Get the population data for a specific ID

### [GET] /population/year/<year>

> Get the population data for a specific year

## Submitting a job (creating a plot)

### [GET] /jobs

> Get all submitted jobs

### [POST] /jobs

> Create a new job (builds a plot)

`curl -X POST --header "Content-Type: application/json" --data '{"start":"1970", "end":"1990"}' localhost:5000/jobs`

### [GET] /jobs/<job_id>

> Get a job for a specific ID

### [DELETE] /jobs/<job_id>

> Delete a job with a specific ID

### [GET] /jobs/<job_id>/plot

> Get the plot created by our worker
